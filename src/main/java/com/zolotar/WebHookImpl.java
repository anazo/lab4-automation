package com.zolotar;

import java.util.ArrayList;
import java.util.List;

public class WebHookImpl implements WebHook {

    private Branch branch;
    private Event.Type type;
    private List<Event> caughtEvents;

    public WebHookImpl(Branch branch, Event.Type type){
        this.branch = branch;
        this.type = type;
        caughtEvents = new ArrayList<>();
    }

    @Override
    public String branch() {
       return branch.toString();
    }

    @Override
    public Event.Type type() {
        return type;
    }

    @Override
    public List<Event> caughtEvents() {
        return List.copyOf(caughtEvents);
    }

    @Override
    public void onEvent(Event event) {
        caughtEvents.add(event);
    }

}
