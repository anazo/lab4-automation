package com.zolotar;

public class GitRepoObservers {

    static private Repository repository;

    public static Repository newRepository() {
       repository =  new RepositoryImpl();
       return repository;
    }

    public static WebHook mergeToBranchWebHook(String branchName) {
      return new WebHookImpl(repository.getBranch(branchName), Event.Type.MERGE);
    }

    public static WebHook commitToBranchWebHook(String branchName) {
        return new WebHookImpl(repository.getBranch(branchName), Event.Type.COMMIT);
    }
}
