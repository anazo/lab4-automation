package com.zolotar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RepositoryImpl implements Repository {

    private List<Branch> branches;
    private List<WebHook> hooks;
    private Branch mainBranch;


    public RepositoryImpl() {
        mainBranch = new Branch("main");
        branches = new ArrayList<>();
        branches.add(mainBranch);
        hooks = new ArrayList<>();
    }

    @Override
    public Branch getBranch(String name) {
        for (int i = 0; i < branches.size(); i++) {
            if (branches.get(i).toString().equals(name))
                return branches.get(i);
        }
        return null;
    }

    @Override
    public Branch newBranch(Branch sourceBranch, String name) {
        if (getBranch(name) != null)
            throw new IllegalArgumentException("A branch with the name " + name + " already exists.");
        if (getBranch(sourceBranch.toString()) == null)
            throw new IllegalArgumentException("A repo doesn't contain branch " + sourceBranch);
        Branch newBranch = new Branch(name);
        for (Commit c : sourceBranch.getCommits())
            newBranch.addCommit(c);
        branches.add(newBranch);
        return newBranch;
    }

    @Override
    public Commit commit(Branch branch, String author, String[] changes) {
        if (getBranch(branch.toString()) == null)
            throw new IllegalArgumentException("A repo doesn't contain a branch " + branch);
        Commit commit = new Commit(author, changes);
        getBranch(branch.toString()).addCommit(commit);
        List<Commit> list = new ArrayList();
        list.add(commit);
        if (!list.isEmpty())
        hooks.stream()
                .filter(x -> (x.type().equals(Event.Type.COMMIT) && x.branch().equals(branch.toString())))
                .forEach(x -> x.onEvent(new Event(Event.Type.COMMIT, branch, list)));
        return commit;

    }

    @Override
    public void merge(Branch sourceBranch, Branch targetBranch) {
        if (getBranch(sourceBranch.toString()) == null || getBranch(targetBranch.toString()) == null)
            throw new IllegalArgumentException("A repo doesn't contain one or both of" +
                    "the following branches: " + sourceBranch + ", " + targetBranch);
        List<Commit> list = sourceBranch.getCommits().stream()
                .filter(x -> targetBranch.getCommits().contains(x) == false)
                .collect(Collectors.toList());
        list.forEach(x -> targetBranch.addCommit(x));
        if (!list.isEmpty())
        hooks.stream().filter(x -> (x.type().equals(Event.Type.MERGE) && x.branch().equals(targetBranch.toString())))
                .forEach(x -> x.onEvent(new Event(Event.Type.MERGE, targetBranch, list)));
    }

    @Override
    public void addWebHook(WebHook webHook) {
        hooks.add(webHook);
    }
}
