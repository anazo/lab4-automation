package com.zolotar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Branch {

    private final String name;
    private List<Commit> commits;
    private List<WebHook> hooks;

    public Branch(final String name) {
        Objects.requireNonNull(name);
        this.name = name;
        commits = new ArrayList<>();
        hooks = new ArrayList<>();
    }

    public boolean addCommit(Commit commit) {
        return commits.add(commit);
    }

    public List<Commit> getCommits() {
        return List.copyOf(commits);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Branch branch = (Branch) o;
        return name.equals(branch.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
